import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SampleClassTest {

    @ParameterizedTest
    @CsvSource({
            "12345678901, true",
            "123456789012, false",
            "12345678981, true",
    })
    void isPeselLengthIsEqualElevenChars(String pesel, boolean result) {
        SampleClass peselValidator = new SampleClass();
        assertThat(peselValidator.validatePesel(pesel)).isEqualTo(result);
    }

    @Test
    void showLongestStringInArray() {
        SampleClass sample = new SampleClass();
        assertEquals(8, sample.longestStringInArray());
    }

    @Test
    void twoPlusFourEqualsSix() {
        SampleClass calculator = new SampleClass();
        assertEquals(6, calculator.addTwoNumbers(2, 4));
    }

    @Test
    void threePlusSevenEqualsTen() {
        SampleClass calculator = new SampleClass();
        assertEquals(10, calculator.addTwoNumbers(3, 7));
    }

    @ParameterizedTest
    @CsvSource({
            "12345678901, kobieta",
            "45678912345, kobieta",
            "87546598731, m�czyzna"
    })
    void validateGenderFromPesel(String pesel, String result) {
        SampleClass genderValidator = new SampleClass();
        assertThat(genderValidator.validateGender(pesel)).isEqualTo(result);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 13, 51322, -513, 0, 53, 100, 555})
    void getMaxValueFromArray(int maxValue) {
        SampleClass maxNumber = new SampleClass();
        assertThat(maxNumber.getMaxValue(maxValue)).isEqualTo(51322);
    }

    @ParameterizedTest
    @CsvSource({
            "12-123, true",
            "123-12, false",
            "1-2345, false"
    })
    void isCorrectPostCodeTest(String postCode, boolean result) {
        SampleClass sampleClass = new SampleClass();
        assertThat(sampleClass.isCorrectPostCode(postCode)).isEqualTo(result);
    }

    @Test
    void fortyEightShouldReturnGradeOne() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(1, gradeCheck.defineNumberGrade(48));
    }

    @Test
    void fiftyOneShouldReturnGradeTwo() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(2, gradeCheck.defineNumberGrade(51));
    }

    @Test
    void sixtySevenShouldReturnGradeThree() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(3, gradeCheck.defineNumberGrade(67));
    }

    @Test
    void seventyNineShouldReturnGradeFour() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(4, gradeCheck.defineNumberGrade(79));
    }

    @Test
    void eightyShouldReturnGradeFive() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(5, gradeCheck.defineNumberGrade(80));
    }

    @Test
    void oneHundredShouldReturnGradeFive() {
        SampleClass gradeCheck = new SampleClass();
        assertEquals(5, gradeCheck.defineNumberGrade(100));
    }

    @Test
    void negativeNumberShouldReturnIllegalArgumentException() {
        SampleClass gradeCheck = new SampleClass();
        assertThrows(IllegalArgumentException.class, () -> {
            gradeCheck.defineNumberGrade(-1);
        });
        // Wewn�trz lambdy powinien znajdowa� si� kod, kt�ry chcemy przetestowa�
    }

    @ParameterizedTest
    @CsvSource({
            "oko, true",
            "morze, false",
            "kajak, true"
    })
    void isPalindromeTest(String word, boolean result) {
        SampleClass checkWord = new SampleClass();
        assertEquals(result, checkWord.isPalindrome(word));
    }

    @ParameterizedTest
    @CsvSource({
            "marcin, 6",
            "ryzen, 5",
            "warszawa, 8",
            "programista, 11"
    })
    void isWordLengthEqualToResult(String word, int result) {
        SampleClass letterCount = new SampleClass();
        assertEquals(result, letterCount.letterCountApp(word));
    }

    @ParameterizedTest
    @CsvSource({
            "6, 21",
            "9, 45",
            "2, 3",
            "4, 10"
    })
    void isNumberCountEqualToResult(int number, int result) {
        SampleClass numberCount = new SampleClass();
        assertEquals(result, numberCount.numbersCountApp(number));
    }

    @ParameterizedTest
    @CsvSource({
            "2, true",
            "3, true",
            "4, false",
            "6, false"
    })
    void isGivenNumberPrimeNumberTest(int number, boolean result) {
        SampleClass primeNumber = new SampleClass();
        assertThat(primeNumber.isPrimeNumber(number)).isEqualTo(result);
    }

    @ParameterizedTest
    @CsvSource({
            "12, 1100",
            "5, 101",
            "183, 10110111"
    })
    void convertIntegerToBinaryTest(int number, String result) {
        SampleClass converter = new SampleClass();
        Assertions.assertThat(converter.convertIntegerNumberToBinaryNumber(number)).isEqualTo(result);
    }

    @Test
    void sortArrayTest() {
        SampleClass sort = new SampleClass();
        int[] expected = new int[]{1, 4, 8, 15};
        assertArrayEquals(expected, sort.sortArray(new int[]{4, 1, 8, 15}));
    }
}