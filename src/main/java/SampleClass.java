import java.util.Arrays;

public class SampleClass {

    public static boolean validatePesel(String pesel) {
        return pesel.length() == 11;
    }

    public static int longestStringInArray() {
        String[] array = {"VCS", "Edit", "Tools", "Window", "Navigate"};
        int max = 0;
        int longestString = array[0].length();
        for (int i = 0; i < array.length; i++) {
            if (array[i].length() > longestString) {
                max = i;
                longestString = array[i].length();
            }
        }
        return array[max].length();
    }

    public static int addTwoNumbers(int number1, int number2) {
        return number1 + number2;
    }

    public static String validateGender(String pesel) {
        char[] chars = pesel.toCharArray();
        if (Integer.parseInt(String.valueOf(chars[9])) % 2 == 0) {
            return "kobieta";
        } else {
            return "m�czyzna";
        }
    }

    public static int getMaxValue(int maxValue) {
        int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555};
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > maxValue) {
                maxValue = numbers[i];
            }
        }
        return maxValue;
    }

    public boolean isCorrectPostCode(String postCode) {
        char[] chars = postCode.toCharArray();
        return Character.isDigit(chars[0]) && Character.isDigit(chars[1]) &&
                chars[2] == '-' && Character.isDigit(chars[3]) &&
                Character.isDigit(chars[4]) && Character.isDigit(chars[5]);
    }

    public int defineNumberGrade(int numberOfPoints) {
        if (numberOfPoints < 0) {
            throw new IllegalArgumentException("Number of points cannot be less than 0");
        }
        if (numberOfPoints < 50) {
            return 1;
        } else if (numberOfPoints < 60) {
            return 2;
        } else if (numberOfPoints < 70) {
            return 3;
        } else if (numberOfPoints < 80) {
            return 4;
        } else {
            return 5;
        }
    }

    public static boolean isPalindrome(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != word.charAt(word.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static int letterCountApp(String word) {
        int letterCount = 0;
        for (int i = 0; i < word.length(); i++) {
            if (Character.isLetter(word.charAt(i))) {
                letterCount++;
            }
        }
        return letterCount;
    }

    public static int numbersCountApp(int number) {
        int counter = 0;
        int sum = 0;
        while (counter <= number) {
            sum += counter;
            counter++;
        }
        return sum;
    }

    public static boolean isPrimeNumber (int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0){
                return false;
            }
        }
        return true;
    }

    public static String convertIntegerNumberToBinaryNumber(int number){
        return Integer.toBinaryString(number);
    }

    public static int[] sortArray (int[] array){
        Arrays.sort(array);
        return array;
    }
}
